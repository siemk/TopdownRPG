//als je in de muur probeert te rennen moet de state switchen naar state idle
if ((abs(input_xaxis) < 0.05 || (abs(input_xaxis) > 0.05 && place_meeting(x + sign(input_xaxis), y, parent_solid))) && (abs(input_yaxis) < 0.05 || (abs(input_yaxis) > 0.05 && place_meeting(x, y + sign(input_yaxis), parent_solid)))) {
    
    script_execute(player_state_idle);
    return true;
    
}

xspd = input_xaxis * walking_maxspeed_x;
yspd = input_yaxis * walking_maxspeed_y;
apply_speed(xspd, yspd);

//sprite handling
sprite_index = spr_red;
