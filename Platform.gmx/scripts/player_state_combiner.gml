
if (input_button_a) { //conditie voor state attack primary
    state = player_state_attack_primary;
} else if (input_button_b) {
    state = player_state_attack_secondary;
} else if (abs(input_xaxis) > 0.05 || abs(input_yaxis) > 0.05) {
    state = player_state_walking;
} else {
    state = player_state_idle;
}

script_execute(state);
