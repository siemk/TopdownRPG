//INPUT
input_left = false;
input_right = false;
input_up = false;
input_down = false;
input_button_a = false;
input_button_b = false;
input_xaxis = 0;
input_yaxis = 0;

//MOVEMENT
xspd = 0;
yspd = 0;
walking_maxspeed_x = 10;
walking_maxspeed_y = 6;

//MISC
state = player_state_idle;
