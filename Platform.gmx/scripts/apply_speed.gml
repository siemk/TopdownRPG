var xspd_ = argument0;
var yspd_ = argument1;

if (!place_meeting(x + xspd_, y, parent_solid)) {
    x += xspd_;
} else {
    while (!place_meeting(x + sign(xspd_), y, parent_solid)) {
        x += sign(xspd_);
    }
}

if (!place_meeting(x, y + yspd_, parent_solid)) {
    y += yspd_;
} else {
    while (!place_meeting(x, y + sign(yspd_), parent_solid)) {
        y += sign(yspd_);
    }
} 
